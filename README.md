# MARRTINO



## Proximal Interaction Robot Coordination Algorithm
Welcome to the README for the Proximal Interaction Robot Coordination Algorithm project. This document provides an overview of the project's goals, methodologies, and implementation strategies.

## Project Overview
The primary objective of this project is to develop a sophisticated algorithm capable of coordinating a robot in a shared environment with humans using proximal interaction. Unlike traditional human-robot interfaces, this project focuses on using human movement as the sole means of interaction, without the need for displays or explicit interfaces. The algorithm seeks to enable seamless collaboration between the robot and humans, especially in scenarios where close proximity is essential.
![image map](results/mymap.png)

## Algorithm Design
Hybrid Control Method
The core of the algorithm is a hybrid control method that combines the robot's perception and human behavior. This fusion is based on a bio-inspired model, drawing inspiration from swarm robotics coordination methods. However, instead of applying these methods to a dynamic swarm, they are adapted for use in a shared environment with a static camera, human subjects, and a single robot. Human feedback also plays a crucial role in refining the coordination process.

## Task Execution
The algorithm's effectiveness is tested through a fundamental task: object manipulation. Specifically, the robot is programmed to identify and grab objects selected by the user. This is achieved through the capture of specific signals obtained from posture estimation, along with data from sensors and cameras mounted on the robot. The robot's behaviors transition smoothly between stages (e.g., approaching the human, interacting with the human, interacting with objects) to achieve the desired task.
![image map](results/Schermata 2022-10-25 alle 14.35.38.png)

## Constraint-Based Approach
To ensure controlled and adaptable behavior, the project employs a constraint-based approach. Constraints are categorized into three main criteria: Communication Method, Control Method, and Task. These constraints are interdependent, with each category developed in harmony with the others. Constraints are defined using Linear Temporal Logic (LTL) and subsequently converted into achievable goals within a simplified planning problem domain representation. Notably, the project defines behaviors as both constraints and goals, leading to dynamic behavior adjustments during execution.


## Repository Structure
The project repository is organized as follows:

src/: Contains the source code for the algorithm and its components.

data/: Holds datasets, recordings, and other necessary data for testing and development.

docs/: Includes additional documentation, research papers, and relevant resources.

examples/: Provides usage examples, demonstrations, and tutorials.
## Getting Started
To experiment with the Proximal Interaction Robot Coordination Algorithm, follow these steps:

Clone the repository: git clone https://gitlab.com/MRomano1/marrtino.git
Explore the source code and examples in the src/ and examples/ directories.
Refer to the documentation in the docs/ directory for in-depth insights and research papers.
Contributions and Support
Contributions to this project are welcome! If you encounter any issues, have ideas for improvements, or want to collaborate, please feel free to submit pull requests or reach out via the issue tracker.
