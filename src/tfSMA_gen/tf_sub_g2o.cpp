#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <iostream>
#include <string> 
#include <fstream>
#include <unistd.h>
#include <cstdlib>
#include <visualization_msgs/Marker.h>
#include <nav_msgs/Odometry.h>
#include <signal.h>
#include <tgmath.h> 
#include <tf2_msgs/TFMessage.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Quaternion.h>
using namespace std;
using std::ofstream;
using std::endl;
using std::cerr;
ofstream outdata;
int i;
double msg_id_app;
double msg_id_x, msg_id_z, msg_id_y;
double msg_r_x, msg_r_y, msg_r_z,msg_id_w;
double app_posy, app_posx, app_posz;
double app_posw;
double app_seq;
double d_incremental;
double y ,p ,r;
double D_posx,D_posy,D_posw;
int sequence=300;
tf2::Transform p0;
tf2::Transform p1;
tf2::Quaternion quat1;
volatile sig_atomic_t stop;
int k=0;
void inthand(int signum) {
    stop = 1;
}

void tfCallback(const tf2_msgs::TFMessage msg){
	  //msg_id_app=msg.transforms[0].child_frame_id;
	  if(i%30==0){
      f_cord_x=
      f_cord_y=
	  outdata <<"VERTEX_XY "<<msg_id_app<<" "<<f_cord_x<<" "<<f_cord_y<<endl;
      cout<<"REGISTRATO: "<< msg_id_app<<endl;
      //cout<<"POS ROBOT: "<<app_posz<<" "<<app_posy<<endl;
	  //cout<<"ID:"<<msg.transforms[0].child_frame_id<<endl;
	  //cout<<"distanza:"<<msg.transforms[0].transform.translation.z<<endl;
	  msg_id_x=msg.transforms[0].transform.translation.z;
	  msg_id_y=-msg.transforms[0].transform.translation.x;
	  msg_id_z=msg.transforms[0].transform.translation.y;
	  msg_r_x=msg.transforms[0].transform.rotation.x;
	  msg_r_y=msg.transforms[0].transform.rotation.y;
	  msg_r_z=msg.transforms[0].transform.rotation.z;
	  msg_id_w=msg.transforms[0].transform.rotation.w;
	  }
}
void imageCallback(const visualization_msgs::Marker msg)
  {
	  if(i%30==0)
	  msg_id_app=msg.id;
	  //msg_id_x=msg.pose.position.x;
	  //msg_id_y=msg.pose.position.y;
	  //msg_id_w=msg.pose.orientation.w;
}
  void odomCallback(const nav_msgs::Odometry msg)
  {
	  if(i%30==0){
	cout<<"VAI!!!"<<msg_id_app<<endl;
	// prendo le posizioni dei tag e della camera nell'ambiente
	//outdata <<"VERTEX_SE2 "<<sequence<<" "<<msg.pose.pose.position.x<<" "<< msg.pose.pose.position.y<<" "<<msg.pose.pose.orientation.w<<endl;
    //outdata <<"EDGE_SE2_XY "<<sequence<<" "<<msg_id_app<<" "<<msg_id_x<<" "<<msg_id_y<<" 1000 0 1000"<<endl;
	p0.setOrigin(tf2::Vector3(app_posz,app_posy,0));
	p0.setRotation(tf2::Quaternion::getIdentity());
	quat1.setRPY(0,0,msg.pose.pose.orientation.w);
	p1.setOrigin(tf2::Vector3(msg.pose.pose.position.x,msg.pose.pose.position.y,0));
	p1.setRotation(quat1);
	auto delta1=p0.inverseTimes(p1);
	D_posx=delta1.getOrigin().getX();
	D_posy=delta1.getOrigin().getY();
	//cout<<"before: "<<app_posz<<endl;
	//cout<<"after: "<<msg.pose.pose.position.x<<endl;
	//cout<<"DELTA: "<<D_posx<<endl;
	//tf2::getEulerYPR(delta1.getRotation(),y,p,r);
	//outdata <<"EDGE_SE2 "<<sequence-1<<" "<<sequence<<" "<<D_posx<<" "<<D_posy<<" "<<msg.pose.pose.orientation.w-app_posw<<" 500 0 0 500 0 5000"<<endl;
	//app_seq=msg.header.seq;
	app_posy=msg.pose.pose.position.y;
	app_posx=msg.pose.pose.position.x;
	app_posz=msg.pose.pose.position.z;
	app_posw=msg.pose.pose.orientation.w;
	sequence++;
}
}

int main(int argc, char** argv){
  signal(SIGINT, inthand);
  ros::init(argc, argv, "tf_converter");
  ros::NodeHandle nh;
  ros::NodeHandle nh2;
  ros::NodeHandle nh3;
  ros::Subscriber sub = nh.subscribe("/visualization_marker",1,imageCallback);
  ros::Subscriber sub2 = nh2.subscribe("/odom",1,odomCallback);
  ros::Subscriber sub3 = nh3.subscribe("/tf",1,tfCallback);
  ros::Rate rate(5);
  outdata.open("test0068_dist.g2o",ios::app);
  i=0;
  while(!stop){
	  //cout << "scritto messaggio: "<< i <<endl;
  ros::spinOnce();
  rate.sleep();
  i++;
}
cout<<"  Fatto!!!  "<<endl;
  outdata.close();
  return 0;
}
