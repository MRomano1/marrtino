#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
using namespace std;
/*
double x_t,y_t,z_t;
double x_r,y_r,z_r,w_r;
void poseCallback(const nav_msgs::Odometry& msg){
	x_t=msg.pose.pose.position.x;
	y_t=msg.pose.pose.position.y;
	z_t=msg.pose.pose.position.z;
	x_r=msg.pose.pose.position.x;
	y_r=
	z_r=
	w_r=
}
*/
int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_broadcaster");
  ros::NodeHandle node;
  //ros::Subscriber sub = node.subscribe("/odom", 10, &poseCallback);
  tf::TransformBroadcaster br;
  tf::Transform transform;

  ros::Rate rate(10.0);
  while (node.ok()){
	//ros::spinOnce();
    transform.setOrigin( tf::Vector3(0.0, 0.0, 0.27) );
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "head_camera"));
    rate.sleep();
  }
  return 0;
};
