(define (problem environment-a-1)
(:domain environment-a)
(:objects
vertex_66
vertex_20
vertex_52
vertex_61
vertex_63
vertex_59
vertex_12
vertex_44
vertex_56
vertex_58
vertex_13
vertex_62
vertex_9
vertex_19
vertex_57
vertex_121
vertex_120
vertex_21
vertex_6
vertex_60
vertex_299
vertex_300
vertex_301
vertex_302
vertex_303
vertex_304
vertex_305
vertex_306
vertex_307
vertex_308
vertex_309
vertex_310
vertex_311
vertex_312
vertex_313
vertex_314
vertex_315
vertex_316
vertex_317
vertex_318
vertex_319
vertex_320
vertex_321
vertex_322
vertex_323
vertex_324
vertex_325
vertex_326
vertex_327
vertex_328
vertex_329
vertex_330
vertex_331
vertex_332
vertex_333
vertex_334
vertex_335
vertex_336
vertex_337
vertex_338
vertex_339
vertex_340
vertex_341
vertex_342
vertex_343
vertex_344

the-gold
the-arrow
agent
human)

(:init
(adj vertex_299 vertex_300) (adj vertex_300 vertex_299) 
(adj vertex_300 vertex_301) (adj vertex_301 vertex_300) 
(adj vertex_301 vertex_302) (adj vertex_302 vertex_301) 
(adj vertex_302 vertex_303) (adj vertex_303 vertex_302) 
(adj vertex_303 vertex_304) (adj vertex_304 vertex_303) 
(adj vertex_304 vertex_305) (adj vertex_305 vertex_304) 
(adj vertex_305 vertex_306) (adj vertex_306 vertex_305) 
(adj vertex_306 vertex_307) (adj vertex_307 vertex_306) 
(adj vertex_307 vertex_308) (adj vertex_308 vertex_307) 
(adj vertex_308 vertex_309) (adj vertex_309 vertex_308) 
(adj vertex_309 vertex_310) (adj vertex_310 vertex_309) 
(adj vertex_310 vertex_311) (adj vertex_311 vertex_310) 
(adj vertex_311 vertex_312) (adj vertex_312 vertex_311) 
(adj vertex_312 vertex_313) (adj vertex_313 vertex_312) 
(adj vertex_313 vertex_314) (adj vertex_314 vertex_313) 
(adj vertex_314 vertex_315) (adj vertex_315 vertex_314) 
(adj vertex_315 vertex_316) (adj vertex_316 vertex_315) 
(adj vertex_316 vertex_317) (adj vertex_317 vertex_316) 
(adj vertex_317 vertex_318) (adj vertex_318 vertex_317) 
(adj vertex_318 vertex_319) (adj vertex_319 vertex_318) 
(adj vertex_319 vertex_320) (adj vertex_320 vertex_319) 
(adj vertex_320 vertex_321) (adj vertex_321 vertex_320) 
(adj vertex_321 vertex_322) (adj vertex_322 vertex_321) 
(adj vertex_322 vertex_323) (adj vertex_323 vertex_322) 
(adj vertex_323 vertex_324) (adj vertex_324 vertex_323) 
(adj vertex_324 vertex_325) (adj vertex_325 vertex_324) 
(adj vertex_325 vertex_326) (adj vertex_326 vertex_325) 
(adj vertex_326 vertex_327) (adj vertex_327 vertex_326) 
(adj vertex_327 vertex_328) (adj vertex_328 vertex_327) 
(adj vertex_328 vertex_329) (adj vertex_329 vertex_328) 
(adj vertex_329 vertex_330) (adj vertex_330 vertex_329) 
(adj vertex_330 vertex_331) (adj vertex_331 vertex_330) 
(adj vertex_331 vertex_332) (adj vertex_332 vertex_331) 
(adj vertex_332 vertex_333) (adj vertex_333 vertex_332) 
(adj vertex_333 vertex_334) (adj vertex_334 vertex_333) 
(adj vertex_334 vertex_335) (adj vertex_335 vertex_334) 
(adj vertex_335 vertex_336) (adj vertex_336 vertex_335) 
(adj vertex_336 vertex_337) (adj vertex_337 vertex_336) 
(adj vertex_337 vertex_338) (adj vertex_338 vertex_337) 
(adj vertex_338 vertex_339) (adj vertex_339 vertex_338) 
(adj vertex_339 vertex_340) (adj vertex_340 vertex_339) 
(adj vertex_340 vertex_341) (adj vertex_341 vertex_340) 
(adj vertex_341 vertex_342) (adj vertex_342 vertex_341) 
(adj vertex_342 vertex_343) (adj vertex_343 vertex_342) 
(adj vertex_343 vertex_344) (adj vertex_344 vertex_343) 
(adj vertex_300 vertex_66)(adj vertex_66 vertex_300) 
(adj vertex_301 vertex_66)(adj vertex_66 vertex_301) 
(adj vertex_302 vertex_9)(adj vertex_9 vertex_302) 
(adj vertex_302 vertex_20)(adj vertex_20 vertex_302) 
(adj vertex_303 vertex_20)(adj vertex_20 vertex_303) 
(adj vertex_304 vertex_20)(adj vertex_20 vertex_304) 
(adj vertex_305 vertex_20)(adj vertex_20 vertex_305) 
(adj vertex_307 vertex_52)(adj vertex_52 vertex_307) 
(adj vertex_308 vertex_52)(adj vertex_52 vertex_308) 
(adj vertex_309 vertex_52)(adj vertex_52 vertex_309) 
(adj vertex_314 vertex_61)(adj vertex_61 vertex_314) 
(adj vertex_315 vertex_61)(adj vertex_61 vertex_315) 
(adj vertex_315 vertex_57)(adj vertex_57 vertex_315) 
(adj vertex_317 vertex_121)(adj vertex_121 vertex_317) 
(adj vertex_319 vertex_121)(adj vertex_121 vertex_319) 
(adj vertex_320 vertex_121)(adj vertex_121 vertex_320) 
(adj vertex_321 vertex_63)(adj vertex_63 vertex_321) 
(adj vertex_324 vertex_12)(adj vertex_12 vertex_324) 
(adj vertex_326 vertex_120)(adj vertex_120 vertex_326) 
(adj vertex_327 vertex_120)(adj vertex_120 vertex_327) 
(adj vertex_328 vertex_44)(adj vertex_44 vertex_328) 
(adj vertex_328 vertex_59)(adj vertex_59 vertex_328) 
(adj vertex_329 vertex_44)(adj vertex_44 vertex_329) 
(adj vertex_330 vertex_44)(adj vertex_44 vertex_330) 
(adj vertex_332 vertex_44)(adj vertex_44 vertex_332) 
(adj vertex_332 vertex_56)(adj vertex_56 vertex_332) 
(adj vertex_332 vertex_21)(adj vertex_21 vertex_332) 
(adj vertex_335 vertex_56)(adj vertex_56 vertex_335) 
(adj vertex_335 vertex_21)(adj vertex_21 vertex_335) 
(adj vertex_337 vertex_58)(adj vertex_58 vertex_337) 
(adj vertex_340 vertex_13)(adj vertex_13 vertex_340) 
(adj vertex_340 vertex_6)(adj vertex_6 vertex_340) 
(adj vertex_340 vertex_60)(adj vertex_60 vertex_340) 
(adj vertex_341 vertex_6)(adj vertex_6 vertex_341) 
(adj vertex_342 vertex_13)(adj vertex_13 vertex_342) 
(adj vertex_343 vertex_13)(adj vertex_13 vertex_343) 
(adj vertex_343 vertex_19)(adj vertex_19 vertex_343) 
(adj vertex_344 vertex_19)(adj vertex_19 vertex_344) 

(pit vertex_66)
(pit vertex_12)
(at the-gold vertex_313)
(at agent vertex_300)
(at human vertex_19))
(:goal (and (have agent the-gold) (at agent vertex_343))))

