(define (domain environment-a)
  ;;(:requirements :strips) 
  (:requirements :strips)

  (:predicates
   (adj ?square-1 ?square-2)
   (pit ?square)
   (at ?what ?square)
   (have ?who ?what)
   (recorded ?who)
   (wait ?who ?where-victim)
   )

  (:action move
    :parameters (?who ?from ?to)
    :precondition (and (adj ?from ?to)
		       (not (pit ?to))
		       (at ?who ?from))
    :effect (and (not (at ?who ?from))
		 (at ?who ?to))
    )

  (:action take
    :parameters (?who ?what ?where ?where-victim)
    :precondition (and (at ?who ?where)
		       (at ?what ?where))
    :effect (and (have ?who ?what)
         (wait ?who ?where-victim)
		 (not (at ?what ?where)))
    )

  (:action record
    :parameters (?who ?where ?victim ?where-victim)
    :precondition (and (at ?who ?where)
		       (at ?victim ?where-victim)
		       (adj ?where ?where-victim))
    :effect (and (recorded ?victim)
         (wait ?who ?where-victim)
		 (not (at ?victim ?where-victim)))
    )
)